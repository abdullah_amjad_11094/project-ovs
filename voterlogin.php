<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>login</title>
  <style media="screen">
      *,
*:before,
*:after{
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}
body{
    background-color: white;
}


form{
    height: 520px;
    width: 400px;
    background-color: rgba(255,255,255,0.13);
    position: absolute;
    transform: translate(-50%,-50%);
    top: 50%;
    left: 50%;
    border-radius: 10px;
    backdrop-filter: blur(10px);
    border: 2px solid rgba(255,255,255,0.1);
    box-shadow: 0 0 40px rgba(8,7,16,0.6);
    padding: 50px 35px;
}
form *{
    font-family: 'Poppins',sans-serif;
    color: #ffffff;
    letter-spacing: 0.5px;
    outline: none;
    border: none;
}
form h3{
    font-size: 32px;
    font-weight: 500;
    line-height: 42px;
    text-align: center;
    color: orange;
}

label{
    display: block;
    margin-top: 30px;
    font-size: 16px;
    font-weight: 500;
    color: orange;
}
input{
    display: block;
    height: 50px;
    width: 100%;
    background-color: rgba(255,255,255,0.07);
    border-radius: 3px;
    padding: 0 10px;
    margin-top: 8px;
    font-size: 14px;
    font-weight: 300;
}
::placeholder{
    color: white;
}
button{
    margin-top: 50px;
    width: 100%;
    background-color:orange;
    color: #080710;
    padding: 15px 0;
    font-size: 18px;
    font-weight: 600;
    border-radius: 5px;
    cursor: pointer;
}
.social{
  margin-top: 30px;
  display: flex;
}
.social div{
  background: red;
  width: 150px;
  border-radius: 3px;
  padding: 5px 10px 10px 5px;
  background-color: rgba(255,255,255,0.27);
  color: #eaf0fb;
  text-align: center;
}
.social div:hover{
  background-color: rgba(255,255,255,0.47);
}
.social .fb{
  margin-left: 25px;
}
.social i{
  margin-right: 4px;
}
.formlogin{
    background: #080710;
}

    </style>

</head>

<?php
date_default_timezone_set("Asia/Karachi");

include("db.php");

$e_id=$_GET['e_id'];

$check="SELECT * FROM election WHERE title='".$e_id."'";

$result=$conn->query($check);

$row=$result->fetch_assoc();

$etime=$row['etime'];
$ctime=date("Y-m-d H:i:s");



if($etime<=$ctime){

  $update = "UPDATE election SET statuss='Ended' WHERE title='".$e_id."'";

  if($conn->query($update)){

    echo'<script>alert("Election Ended!");
    window.location.href="g.php";
    </script>';

		

  }


}




?>



<body>
<!-- partial:index.partial.html -->


    <div class="hang">
    <form class="formlogin" action="voterlogin.php?e_id=<?php echo($e_id);?>" method="post">
        <h3>Login</h3>

        <label for="username">Username</label>
        <input type="text" placeholder="ID" name="username" required>

        <label for="password">Password</label>
        <input type="password" placeholder="Password" name="password" required>

        <button type="submit" name="submit" >Log In</button>
        
    </form>
</div>


<?php


if(isset($_POST['submit'])){


  $id=$_POST['username'];
  $pas=$_POST['password'];

  $login="SELECT * FROM votters WHERE idd='".$id."' AND keyy='".$pas."' AND e_id='".$e_id."' AND statuss='"."unvotted"."'";

  $resultlogin=$conn->query($login);

  if($resultlogin->num_rows>0){

//redirect to ballot and pass e_id and votterid


 header("location: ballot_paper.php?e_id=".$e_id."&v_id=".$id."");


  }else{


    echo'<script>alert("Acess Denied!");
    reload();
    </script>';
    // redirect to same page!

   
    
  }





}



?>
<script>

  function reload(){

    var id="<?php echo($e_id)?>";
 window.location.href="voterlogin.php?e_id".concat(id);


  }

  

</script>
</body>
</html>
