

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/major.css">
	<title>Dashboard</title>
	<script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js">
      </script>
      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>
</head>
<body>

<header>

<?php

include("header.php")

?>

</header>
<div>
<?php
include("side_nav2.php")
?>
</div>
<?php
include("../db.php");
$e_id=$_GET['e_id'];

$sql="SELECT * FROM election WHERE title='".$e_id."'";
$result=$conn->query($sql);
$row=$result->fetch_assoc();

?>

<div class="show_election">
<div>
    <label>Election Title : <?php echo(" ".$row['title'])?> </label>
</div>
    <br>
	<div>
    <label>Election Type:<?php echo(" ".$row['_type'])?> </label>
</div>
    <br>
	<div>
    <label>No of candidates:<?php echo(" ".$row['cand'])?> </label>
</div>
</div>

</div>
<div class="allowed">
    <span class="allo">Votters Allowed :<?php echo(" ".$row['votters'])?> </span>
</div>


<div class="graph">
<!-- GRAPH  -->
<div id = "container" style = "width: 550px; height: 350px; margin: 16px 0px 0px 523px">

<script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
				['_name', 'votes'],
				
                <?php  
            
                include("../db.php");
                $sqll="select _name,votes from candidates where e_id='".$e_id."'";
                $res=mysqli_query($conn,$sqll);
                while($resultt=mysqli_fetch_assoc($res))
                {
					
                     echo"['".$resultt['_name']."',".$resultt['votes']."],";
                }

				?>
            ]);

            var options = {title: 'Results'
			}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('container'));
			
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
		 
      </script>

      </div>
	    <!-- <script src="js/bar-multicolor.js"></script> -->

</div>

<div>
<?php
include("footer.php")
?>
</div>
</body>
</html>


