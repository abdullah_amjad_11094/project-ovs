

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/major.css">
	<title>Approvals</title>
</head>
<body>

<header>

<?php

include("header.php")

?>

</header>
<div>
<?php
include("side_nav2.php")
?>
</div>

<div class="approval_table">
    <table id="tabledata">
		<thead>
        <tr>
                <th>Name</th>
                <th>Amount</th>
                <th>Account No</th>
                <th>Method</th>
                <th>Proff</th>
                <th>Approve</th>
        </tr>
		</thead>
		<tbody>
        <?php
    include("../db.php");

    $sql="SELECT * FROM approvals WHERE statuss='Pending'";

    $result=$conn->query($sql);

    if($result->num_rows>0){


        while($row=$result->fetch_assoc()){

   
    
    ?>
<tr>
    <td><?php echo($row['_name'])?></td>
    <td><?php echo($row['amount'])?></td>
    <td><?php echo($row['account_no'])?></td>
    <td><?php echo($row['method'])?></td>
    <td><a href="../<?php echo($row['proff'])?>" target="_blank">File</a></td>
    <td><button class="Approve-button" data-name="<?php echo($row['id'])?>" onclick="approve(this)">Approve</button></td>

</tr>

<?php 

} }

?>



		</tbody>
	</table>
</div>


<div>
<?php
include("footer.php")
?>

<script>
    function approve(e){


        var id=e.getAttribute("data-name");
        let link="approve.php?p_id="
        let flink=link.concat(id);
        window.location.href=flink;


    }
</script>
</div>
</body>
</html>


