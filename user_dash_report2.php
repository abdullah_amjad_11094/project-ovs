<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/major.css">
    <title>Report</title>
    <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js">
      </script>
      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>

</head>
<body>
    
<?php
session_start();
   ?>
<header>
<?php
include("header.php")
?>
</header>


<div>
<?php
include("side_nav.php")
?>
</div>


<div class="r_table" >

    <table id="tabledata" >
    <thead>
        <tr>
        <th>Candidate</th>
        <th>Votes</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>


    <?php 
        
        include("db.php");
        
        $e_id=$_GET['e_id'];
        $sql="SELECT * FROM candidates WHERE e_id='".$e_id."'";

        $result=$conn->query($sql);

        if($result->num_rows>0){

            while($row=$result->fetch_assoc()){


       
        
        
        ?>

       <tr> 
            <td><?php echo($row['_name'])?></td>
            <td><?php echo($row['votes'])?></td>
            <td><?php echo($row['status'])?></td>
        </tr>
      
       
    
<?php

            }
        
        }
?>

    </tbody>

    </table>
</div>

<div class="graph">
<!-- GRAPH  -->
<div id = "container" style = "width: 550px; height: 400px; margin: -408px 0px 0px 879px">
      </div>
      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
				['_name', 'votes'],
				
                <?php  
                
                include("db.php");
                $sql="select _name,votes from candidates where e_id='".$e_id."'";
                $res=mysqli_query($conn,$sql);
                while($result=mysqli_fetch_assoc($res))
                {
					
                     echo"['".$result['_name']."',".$result['votes']."],";
                }
?>
            ]);

            var options = {title: 'Results'
			}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('container'));
			
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
		 
      </script>
	    <script src="js/bar-multicolor.js"></script>

</div>


<div>
    <?php
    
    include("footer.php")
    ?>
</div>
</body>
</html>