<link rel="stylesheet" type="text/css" href="css/footer.css">
<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>

<footer class="footer-distributed">

<div class="footer-right">

    <a href="#"><i class='bx bxl-facebook-square'></i></a>
    <a href="#"><i class='bx bxl-instagram' ></i></a>
    <a href="#"><i class='bx bxl-github' ></i></a>
    <a href="#"><i class='bx bxl-linkedin' ></i></a>    
    <p>
        Developed by : Abdullah Amjad & Ibrahim Ahmed
    </p>
</div>

<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="index.php">Home</a>
        <a href="index.php#section1">Packages</a>
        <a href="index.php#section2">About</a>
    </p>

    <p>OVS &copy; 2021</p>
</div>

</footer>

</footer>

