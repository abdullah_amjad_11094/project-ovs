<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Packages</title>
</head>
<body>
    <header>
        <?php
		include("header.php")
		?>
    </header>
	<div>
       <?php include("side_nav.php")?>
	</div>
<div>

	<table class="package" id="tabledata">
		<thead>
			<tr>
				<th>Name</th>
				<th>Package</th>
				<th>Price (PKR)</th>
				<th></th>
			</tr>
		</thead>
		<tbody>

		<?php
    
    include("db.php");
    $sql="SELECT * FROM packages";
	session_start();

	$bal="SELECT * FROM wallet WHERE email='".$_SESSION['email']."'";

	$resultbal=$conn->query($bal);
	$row=$resultbal->fetch_assoc();
	$temp_bal=$row['Amount'];

    $result=$conn->query($sql);

    if($result->num_rows>0){


        while($row=$result->fetch_assoc()){

   
    
    ?>

			<tr>
			<td><?php echo($row['_name']);?></td>
			<td><?php echo($row['voters']);?></td>
			<td><?php echo($row['price']);?></td>
			<td><button class="buy-button" data-bal="<?php echo($temp_bal)?>" data-id="<?php echo($row['id'])?>" data-price="<?php echo($row['price'])?>" onclick="Buy(this)">Buy</button></td>
			</tr>


<?php
		
	}
}
	?>

		</tbody>

		
	</table>
</div>
<?php 
include("footer.php")
?>

<script>
    function Buy(e){


        let id=e.getAttribute("data-id");
		let price=Number(e.getAttribute("data-price"));
		let bal=Number(e.getAttribute("data-bal"));
		
		if(bal>=price){
		let link="user_dash_create_election.php?p_id=";
        let flink=link.concat(id);

        window.location.href=flink.concat("&bal=",bal);
		}else{

			alert("You Dont have enough Balance!");
			
			window.location.href="user_dashboard.php";

		}
        

    }
</script>

</body>
</html>