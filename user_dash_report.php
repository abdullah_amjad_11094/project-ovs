<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/major.css">
    <title>Report</title>
</head>
<body>
    <header>
    <?php
    session_start();
    include("header.php")
    ?>
    </header>
    <div>
        <?php
        include("side_nav.php")
        ?>
    </div>

    <!--Search bar -->
    <div class="sp">
        <form action="">
        <input type="text" name="search" <?php if(isset($_GET['search'])){ echo($_GET['search']); } ?> placeholder="Search...." id="searchi">
        <button type="submit"  class="search" id="searchb">Search</button>
        </form>
  
    </div>
<!--Table -->
   <div  class="tablle" >
        <table id="tabledata">
        <thead>
         <tr>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Status</th>
            <th>Winner</th>
        </tr>
        </thead>
        <tbody>
        <?php
            include("db.php");
            if(isset($_GET['search']))
            {
                $filtervalue=$_GET['search'];
                $query="select * from election where concat(title) like '%$filtervalue%' ";
                $query_run=mysqli_query($conn, $query);

                if(mysqli_num_rows($query_run) > 0)
                {
                    foreach($query_run as $row)
                    {
                        
                        ?>
        <!-- explore function should be edited-->
        <tr class="election" data-title="<?php echo($row['title'])?>" onclick="explore(this)"> 
            <td><?php echo($row['title'])?></td>
            <td><?php echo($row['stime'])?></td>
            <td><?php echo($row['etime'])?></td>
            <td><?php echo($row['statuss'])?></td>
            <td><?php echo($row['Winner'])?></td>
            
        </tr>
      
    
        <?php
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td colspan="5">No record found</td>
                </tr>
                    <?php
                }
            }

else{
    ?>

    
<?php 
        
        include("db.php");

        $m=$_SESSION['email'];
        $_SESSION['e_id']=NULL;
        $sql="SELECT * FROM election WHERE _owner='".$m."'";

        $result=$conn->query($sql);

        if($result->num_rows>0){

            while($row=$result->fetch_assoc()){


       
        
        
        ?>
        <!-- explore function should be edited-->
        <tr class="election" data-title="<?php echo($row['title'])?>" onclick="explore(this)"> 
            <td><?php echo($row['title'])?></td>
            <td><?php echo($row['stime'])?></td>
            <td><?php echo($row['etime'])?></td>
            <td><?php echo($row['statuss'])?></td>
            <td><?php echo($row['Winner'])?></td>
            
        </tr>
      
    <?php
            }
             
}

}


    ?>

        </tbody>

        </table>
   </div>

<?php

include("css/table.js")

?>

    <!-- Footer -->
  <?php
    include("footer.php")
    ?>


<script>

function explore(e){

    let e_id=e.getAttribute("data-title");
    
    let link="user_dash_report2.php?e_id=";
    let link2=link.concat(e_id);
    window.location.href = link2;


}

</script>

</body>

</html>